package main

import (
	"file/filepath"
	"fmt"
	"os"
	"os/user"
	"strings"
	// "bitbucket.org/cordelltech/smartedgecodechallenge/codeChallenge/keys"
)

func main() {
	expArgs := 1
	s := validateArgs(expArgs, os.Args)[expArgs]

	pub, prv := getKeyPaths()

	// pub, prv, err := keys.GetKeyPair(privateKeyPath, publicKeyPath)
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// fmt.Println("pub", pub)
	// fmt.Println("priv", prv)
}

// validateArgs will check if correct number of args, as indicated with the passed
// func argument expArgs, was passed to the program and check validity
// if invalid, the program will exit with a descriptive error else the space trimed argument is returned
func validateArgs(expArgs int, args []string) []string {
	if len(os.Args) != expArgs+1 {
		fmt.Printf("error: invalid number of arguments - exactly %d is required\n", expArgs)
		os.Exit(1)
	}

	aa := make([]string, expArgs)

	// remove spaces and check if arg length at least one
	for i, a := range args[1:] {
		aa[i] = strings.TrimSpace(a)
		if len(aa[i]) == 0 {
			fmt.Println("error: invalid argument - an empty string is not allowed")
			os.Exit(1)
		}
	}
	return aa
}

func getKeyPaths() (string, string) {
	u, err := user.Current()
	if err != nil || len(u.HomeDir) == 0 {
		fmt.Printf("error: could not get user's home dir, %v\n", err)
		os.Exit(1)
	}

	storeRootDir := filepath.Join(u.HomeDir, "codechallenge")
	return filepath.Join(storeRootDir, "public.key"), filepath.Join(storeRootDir, "private.key")
}

// Properties is the model for the JSON schema properties object
type Properties struct {
	Message   string `json:"message"`
	Signature string `json:"signature"`
	Pubkey    string `json:"pubkey"`
}
