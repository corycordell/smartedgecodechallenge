FROM golang:alpine as build
WORKDIR /app
COPY pkg/ ./
ENV CGO_ENABLED=0
RUN go test -v -coverprofile cp.out