package keys

import (
	"crypto/rand"
	"crypto/rsa"
	"os/user"
	"path/filepath"

	// "crypto/sha512"
	// "crypto/x509"
	"encoding/asn1"
	// "encoding/pem"
	"fmt"
	"io/ioutil"
	"os"
)

var (
	// expArgs is the expected number of arguments after the root command
	expArgs = 1
	// bitSize is the size of the key
	bitSize = 2048
	// directory in which to store app data
	storeRootDir string
	// public and private key filenames
	privateKeyFilename, publicKeyFilename string
	// path relative to the storeRootDir in which to save the public and private keys
	privateKeyPath, publicKeyPath string
)

// New will return a KeyPair or an error when provided
func New(size int) (*KeyPair,error) {
	
	kp := &KeyPair{
		PrivateKeyPath: vpath,
		PublicKeyPath: bpath,
	}
	return kp, nil
}

// KeyPair manages an RSA key pair
type KeyPair struct {
	PublicKey      *rsa.PublicKey
	PrivateKey     *rsa.PrivateKey
	PrivateKeyPath string
	PublicKeyPath  string
}

// GetKeyPair will get the keypair from the filesystem if it exists
// if not it will be created and stored in the specied location
func getKeyPair(prvKeyPath, pubKeyPath string) (pub *rsa.PublicKey, prv *rsa.PrivateKey, err error) {
	prvf, err := ioutil.ReadFile(prvKeyPath)
	if err != nil {
		return nil, nil, err
	}
	fmt.Println(prvf)
	pubf, err := ioutil.ReadFile(pubKeyPath)
	if err != nil {
		return nil, nil, err
	}
	asn1.Marshal(pubf)
	fmt.Println(pubf)
	return pub, prv, nil
}

// generateKeyPair will get the keypair from the filesystem if it exists
// if not it will be created and stored in the specied location
func generateKeyPair(size int) (*rsa.PublicKey, *rsa.PrivateKey) {
	r := rand.Reader
	prv, err := rsa.GenerateKey(r, size)
	if err != nil {
		fmt.Println("internal error: the program could not generate a private key")
		os.Exit(1)
	}

	return &prv.PublicKey, prv
}

// // Encrypt encrypts the passed message with the passed private key
// func Encrypt(msg []byte, key *rsa.PrivateKey) []byte {
// 	hash := sha512.New()
// 	ciphertext, err := rsa.EncryptOAEP(hash, rand.Reader, key, msg, nil)
// 	if err != nil {
// 		fmt.Println("internal error: could not encrypt message")
// 		os.Exit(1)
// 	}
// 	return ciphertext
// }

// func PrvToBytes(prv *rsa.PrivateKey) error {
// 	privBytes := pem.EncodeToMemory(
// 		&pem.Block{
// 			Type:  "RSA PRIVATE KEY",
// 			Bytes: x509.MarshalPKCS1PrivateKey(prv),
// 		},
// 	)
// 	return nil
// }

// // Save will save content to a file
// func Save(b []byte, path string) error {

// 	return nil
// }

// // PrivateKeyToBytes private key to bytes
// func PrivateKeyToBytes(priv *rsa.PrivateKey) []byte {

// 	return []byte{}
// }

// // PublicKeyToBytes public key to bytes
// func PublicKeyToBytes(pub *rsa.PublicKey) []byte {
// 	pubASN1, err := x509.MarshalPKIXPublicKey(pub)
// 	if err != nil {
// 		fmt.Println("internal error: could not convert key to bytes")
// 		os.Exit(1)
// 	}

// 	pubBytes := pem.EncodeToMemory(&pem.Block{
// 		Type:  "RSA PUBLIC KEY",
// 		Bytes: pubASN1,
// 	})

// 	return pubBytes
// }
